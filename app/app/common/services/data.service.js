(function() {
    'use strict';

    angular
        .module('core')
        .service('dataService', dataService);

    dataService.$inject = ['$resource', 'API'];
    
    function dataService($resource, API) {
        return $resource(`${API.cars}/:id`, {
            id: '@id'
        }, {
            update: {method:'PUT'}
        })
    }
})();

