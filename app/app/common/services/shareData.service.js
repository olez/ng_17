(function() {
    'use strict';

    angular
        .module('core')
        .service('shareData', shareData);
    
    function shareData() {
        return {
            search: ''
        };
    }
})();

