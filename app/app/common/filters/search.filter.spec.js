describe('hiddenFilter: ', () => {
    var $filter;

    beforeEach(module('core'));

    beforeEach(inject($injector => {
        $filter = $injector.get('$filter');
    }));

    var collection = [{
        id: 0,
        title: 'item 0',
        hidden: false
    },
    {
        id: 1,
        title: 'item 1',
        hidden: true
    },
    {
        id: 2,
        title: 'item 2',
        hidden: false
    },
    {
        id: 3,
        title: 'item 3',
        hidden: true
    },
    {
        id: 4,
        title: 'item 4',
        hidden: false
    }
];
    var property = 'hidden';

    it('should filter the collection', () => {
        var result = $filter('hiddenFilter')(collection, property, 'all');
        expect(result.length).toBe(5);
    });
    it('should filter the collection for only visible', () => {
        var result = $filter('hiddenFilter')(collection, property, false);
        expect(result.length).toBe(3);
    });
    it('should filter the collection for only hidden', () => {
        var result = $filter('hiddenFilter')(collection, property, true);
        expect(result.length).toBe(2);
    });
})