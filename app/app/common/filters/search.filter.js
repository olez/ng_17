(function () {
    'use strict';

    angular
        .module('core')
        .filter('hiddenFilter', hiddenFilter);

    function hiddenFilter() {
        return (collection, property, value) => 
            (!property || value === 'all') ? collection : collection.filter( item => item[property] === value );
    }
})();

