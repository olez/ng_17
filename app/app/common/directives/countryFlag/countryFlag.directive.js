(function() {
    'use strict';

    angular
        .module('core')
        .directive("countryFlag", countryFlag);
    
    function countryFlag (){
        return {
            restrict: 'A',
            controller: 'countryFlagCtrl as ctrl',
            scope: true,
            compile: function(scope, element, attrs, ctrl){
                return {
                    post: function postLink(scope, element, attrs, ctrl){
                        ctrl.prependImg(attrs.countryFlag, element);
                    }
                }
            }
        }   
    }
})();

