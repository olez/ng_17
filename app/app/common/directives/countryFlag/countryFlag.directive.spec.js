describe('countryFlag.directive', function(){
    var $rootScope, $compile, $httpBackend;

    beforeEach(module('root'));

    beforeEach(function(){
            var countryServiceMock = function (){};

            countryServiceMock.getList = function(){
                return {
                    $promise: {
                        then: function(callback){
                            callback({
                                "Ukraine": "/svg/UA.svg",
                                "Uganda": "/svg/UG.svg",
                                "US Minor Outlying Islands": "/svg/UM.svg",
                                "United States": "/svg/US.svg"
                            })
                        }
                    }
                }
            };

            countryServiceMock = countryServiceMock.getList();
            
            module(function ($provide) {
                $provide.value('countryService', countryServiceMock);
            });    
    });

    beforeEach(inject(($injector) => {
        $controller = $injector.get('$controller');
        $compile = $injector.get('$compile');
        $rootScope = $injector.get('$rootScope');
        $httpBackend = $injector.get('$httpBackend');
    }));

it('should append child flag image', function(){
    $httpBackend.when('GET', 'assets/flags/svg/UA.svg')
        .respond(200, '');
    $httpBackend.when('GET', './app/component/cars/cars-list/cars-list.template.html')
        .respond(200, '');
        
    var element = angular.element('<li country-flag="Ukraine"></li>');
    $compile(element)($rootScope.$new());
    $rootScope.$digest();

    expect(element.html()).toContain('<img src="assets/flags/svg/UA.svg">');      
})

it('should append child Uganda flag image', function(){
    $httpBackend.when('GET', 'assets/flags/svg/UG.svg')
        .respond(200, '');
    $httpBackend.when('GET', './app/component/cars/cars-list/cars-list.template.html')
        .respond(200, '');
        
    var element = angular.element('<li country-flag="Uganda"></li>');
    $compile(element)($rootScope.$new());
    $rootScope.$digest();
    expect(element.html()).toContain('<img src="assets/flags/svg/UG.svg">');      
})

it('should append child United States flag image', function(){
    $httpBackend.when('GET', 'assets/flags/svg/US.svg')
        .respond(200, '');
    $httpBackend.when('GET', './app/component/cars/cars-list/cars-list.template.html')
        .respond(200, '');
        
    element2 = angular.element('<li country-flag="United States"></li>');
    $compile(element2)($rootScope.$new());
    $rootScope.$digest();
    expect(element2.html()).toContain('<img src="assets/flags/svg/US.svg">');         
})
})


