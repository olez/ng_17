(function(){
    angular
        .module('core')
        .controller('countryFlagCtrl', countryFlagCtrl);

    countryFlagCtrl.$inject = ['countryService'];

    function countryFlagCtrl(countryService){
        let ctrl = this;
            ctrl.prependImg = initFlag;

        function initFlag (query, element) {
            countryService.$promise.then( countries => prependFlag(countries[query], element));
        }

        function prependFlag(img, element) {
            var imgElem = angular.element("<img>");
            imgElem[0]['src'] = `assets/flags${img}`;

            element.prepend(imgElem);
        }
    }
})();