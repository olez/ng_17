(function() {
    'use strict';

    angular
        .module('core')
        .service('countryService', countryService);

    countryService.$inject = ['$resource', 'API'];

    function countryService($resource, API) {
        return $resource(API.countries, {}, {
            getList: {method:'GET'}}).getList();
    }
})();