(function () {
    'use strict';

    angular
        .module('core')
        .controller('NavbarController', NavbarController);

    NavbarController.$inject = ['shareData'];

    function NavbarController(shareData) {
        var vm = this;
        vm.data = shareData;
    }
})();

