(function () {
    'use strict';

    angular
        .module('cars')
        .controller('carCardController', carCardController);

    function carCardController(){
        let ctrl = this;
        ctrl.deleteCar = deleteCar;
        ctrl.hide = hideCar;

        function deleteCar(car) {
            car.$delete();
        }

        function hideCar(car) {
            car.hidden = !car.hidden;
            car.$update();
        }
    }
})();

