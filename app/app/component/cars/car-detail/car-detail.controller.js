(function () {
    'use strict';

    angular
        .module('cars')
        .controller('carDetail', carDetail);

    carDetail.$inject = ['$routeParams', 'dataService'];

    function carDetail($routeParams, dataService) {
        var vm = this;
            vm.id = $routeParams.id;
            vm.car = dataService.get({id: vm.id});
    }

})();

