(function () {
    'use strict';

    angular
        .module('cars')
        .controller('addForm', addForm);

    addForm.$inject = ['$location', 'dataService'];

    function addForm($location, dataService) {
        var vm = this;
            vm.car = {};
            vm.save = addCar;
    
        function addCar(car) {
            let newCar = new dataService();
            Object.assign(newCar, car);
            newCar.image ? '' : newCar.image = 'assets/img/no-picture.png';
            newCar.hidden= false;
        
            newCar.$save(()=>{
                $location.url('/cars');
            });
        }
    }

})();

