(function () {
    'use strict';

    angular
        .module('cars')
        .controller('carEdit', carEdit);

    carEdit.$inject = ['$routeParams', 'dataService', '$location'];

    function carEdit($routeParams, dataService, $location) {
        var vm = this;
            vm.id = $routeParams.id;
            vm.car = dataService.get({id: vm.id});
            vm.update = updateCar;
        
        function updateCar(car){
            car.$update(_ => $location.url('/cars'));
        }
    }

})();

