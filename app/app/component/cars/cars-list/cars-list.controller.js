(function () {
    'use strict';

    angular
        .module('cars')
        .controller('CarsController', CarsController);

    CarsController.$inject = ['dataService', 'shareData'];
    
    function CarsController(dataService, shareData) {
        var vm = this;
            vm.cars = dataService.query();

            vm.data = shareData,
            vm.state = false;
            vm.setState = setState;

        function setState(value){
            vm.state = value;
        }
    }

})();

