describe('CarsController', function(){
    var $controller, CarsController;
    //$controller - fetch the controller's service

    beforeEach(module('root'));

    beforeEach(inject(function($injector){
        //$injector - allows us to fetch dependencies from our app
        $controller = $injector.get('$controller');
        
        var $scope = {};

        // mock dataService
        var dataService = function () {};

        dataService.query = function(){
            return [{
                        "name": "AC Cobra",
                        "country": "United States",
                        "top_speed": "160",
                        "zero_to_sixty": "4.2",
                        "power": "485",
                        "engine": "6997",
                        "weight": "1148",
                        "description": "The AC Cobra, sold as the Ford/Shelby AC Cobra in the USA and often known colloquially as the Shelby Cobra in that country, is an American-engined British sports car produced intermittently since 1962.",
                        "image": "assets/img/005.png",
                        "id": 0,
                        "hidden": true
            }];
        };

        //instantiate the controller
        CarsController = $controller('CarsController', {
            $scope,
            dataService
            });
        }))

        it('should get cars array from the service', function () {
            expect(CarsController.cars[0]).toEqual({
                "name": "AC Cobra",
                "country": "United States",
                "top_speed": "160",
                "zero_to_sixty": "4.2",
                "power": "485",
                "engine": "6997",
                "weight": "1148",
                "description": "The AC Cobra, sold as the Ford/Shelby AC Cobra in the USA and often known colloquially as the Shelby Cobra in that country, is an American-engined British sports car produced intermittently since 1962.",
                "image": "assets/img/005.png",
                "id": 0,
                "hidden": true
            });
        });
})