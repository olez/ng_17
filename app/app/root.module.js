(function () {
    'use strict';

    angular
        .module('root', [
            'ngRoute',
            'ngResource',
            /**/
            'core',
            'components'
        ])
        .config(config)
        .constant('API', {cars:'http://localhost:3000/cars', countries: 'http://localhost:3000/countries'});

    config.$inject = ['$routeProvider'];
    
    function config($routeProvider) {
        $routeProvider
            .when('/cars', {
                templateUrl: './app/component/cars/cars-list/cars-list.template.html',
            })
            .when('/car/:id', {
                templateUrl: './app/component/cars/car-detail/car-detail.template.html'
            })
            .when('/cars/new', {
                templateUrl: './app/component/cars/car-new/car-new.template.html'
            })
            .when('/cars/edit/:id', {
                templateUrl: './app/component/cars/car-edit/car-edit.template.html'
            })
            .otherwise('/cars');
    }

})();