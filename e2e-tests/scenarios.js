'use strict';

describe('CDS Application', function() {

  describe('carsList', function() {

    beforeEach(function() {
      browser.get('#!/cars');
    });

    it('should filter the cars list as a user types into the search box', function() {
      var carsList = element.all(by.repeater('car in vm.cars'));
      var query = element(by.model('vm.data.search'));

      expect(carsList.count()).toBe(29);

      query.sendKeys('ferrari');
      expect(carsList.count()).toBe(2);

      query.clear();
      query.sendKeys('porsche');
      expect(carsList.count()).toBe(3);
    });

  });

});