//jshint strict: false
module.exports = function(config) {
  config.set({

    basePath: './app',

    files: [
      './lib/angular/angular.js',
      './lib/angular-mocks/angular-mocks.js',
      './lib/angular-route/angular-route.js',
      './lib/angular-resource/angular-resource.js',
      './app/root.module.js',
      './app/root.directive.js',
      './app/common/core.module.js',
      './app/common/layout/navbar.controller.js',
      './app/common/layout/navbar.directive.js',
      './app/common/directives/countryFlag/countryFlag.controller.js',
      './app/common/directives/countryFlag/countryFlag.directive.js',
      './app/common/services/data.service.js',
      './app/common/directives/countryFlag/country.service.js',
      './app/common/services/shareData.service.js',
      './app/common/filters/search.filter.js',
      './app/component/components.module.js',
      './app/component/cars/cars.module.js',
      './app/component/cars/cars-list/cars-list.controller.js',
      './app/component/cars/car-card/car-card.controller.js',
      './app/component/cars/car-card/car-card.directive.js',
      './app/component/cars/car-detail/car-detail.controller.js',
      './app/component/cars/car-edit/car-edit.controller.js',
      './app/component/cars/car-new/car-new.controller.js',
      //tests

      './app/common/directives/countryFlag/countryFlag.directive.spec.js',
      './app/component/cars/cars-list/cars-list.controller.spec.js',
      './app/common/filters/search.filter.spec.js'
    ],

    autoWatch: true,

    frameworks: ['jasmine'],

    browsers: ['Chrome'],

    plugins: [
      'karma-chrome-launcher',
      'karma-firefox-launcher',
      'karma-jasmine',
      'karma-spec-reporter',
    ],
    
    singleRun: true,

    reporters: ['spec']

  });
};
